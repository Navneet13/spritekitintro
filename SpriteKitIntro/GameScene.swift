//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by Navpreet Kaur on 2019-09-30.
//  Copyright © 2019 Navneet. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    //ALL GAME LOGIC GOES HERE

    //MARK: Sprite  Variables
    var pikachu: SKSpriteNode!
    var square: SKSpriteNode!
    var highScoreLabel:SKLabelNode!

    //CONSTRUCTOR: setup your scene and sprites
    override func didMove(to view: SKView) {

        //size is a global variable that gives you the size of the screen.
        //size.width is same as screenWidth in android and same thing with size.height (screenHeight)
        print("Screen size:\(size.width),\(size.height)")
        
        
    //MARK: MAKE SOME TEXT
        //---------------
        //1. Make a label node
         self.highScoreLabel = SKLabelNode(text: "Score:25")
        
        //2.  Configure the node
        //(Setting the font size, color, postion etc
        self.highScoreLabel.position = CGPoint(x: 100,y: 100)
        self.highScoreLabel.fontSize = 45
        self.highScoreLabel.fontColor = UIColor.yellow
        self.highScoreLabel.fontName = "Avenir"
        
        //3. Show the node on the screen
        addChild(self.highScoreLabel)
        
        
    //MARK: DRAW SQAURES
        //----------
        
        //1. Make a square node
        //NOTE: CGSize objects = Rect in android
        self.square = SKSpriteNode(color: UIColor.yellow, size: CGSize(width: 100, height: 100))
        
        //2. Configure the sqaure
        self.square.position = CGPoint(x: 300, y: 200)
        
        //3. Add the sqaure to the screen
        addChild(self.square)
        
        
        
     //MARK: DRAW IMAGES ON THE SCREEN
        //-----------
        
        //1. Make an image node
        self.pikachu = SKSpriteNode(imageNamed: "pikachu")
        
        //2. configure the image node
        self.pikachu.position = CGPoint(x:200,y:323)
        
        //3. Add to screen
        addChild(self.pikachu)
        
        
        //3. Use automatic movement to build a sequence
        let upMoveAction = SKAction.moveBy(x:0, y:50, duration:4)
        let leftMoveAction = SKAction.moveBy(x: -100, y: 0, duration: 1)
        let rightMoveAction = SKAction.moveBy(x: 50, y: 0, duration: 1)
        
        let animation = SKAction.sequence([upMoveAction,leftMoveAction,rightMoveAction,upMoveAction])
        self.square.run(animation)
        
        //do it once
        self.highScoreLabel.run(animation)
        
        //do it 3 times
        let threeTimesAction = SKAction.repeat(animation,count: 3)
        self.square.run(threeTimesAction)
        
        //move the square label forever
        let scoreLabelAnimation = SKAction.sequence([rightMoveAction,upMoveAction])
        
        let foreverAnimation = SKAction.repeatForever(scoreLabelAnimation)
        self.highScoreLabel.run(foreverAnimation)
    }
    
    
    //MARK: UPDATE POSITIONS / REDRAW SPRITES FUNCTION
    
    var pikachuDirection: String = "left"
    
    override func update(_ currentTime: TimeInterval) {
        //Built-in function that runs once per frame
        //Similar to updatePositions() in android
        
        //TYPES OF MOVEMENTS:
        //------------
        //1. Manual Movement   (Similar to Android)
        if(self.pikachuDirection == "right")
        {
            self.pikachu.position.x = self.pikachu.position.x + 1;
            
            //Check if pikachu touches the wall
            if(self.pikachu.position.x >= size.width){
               self.pikachuDirection  = "left"
            }
        }
        else
        if(self.pikachuDirection == "left")
        {
            self.pikachu.position.x = self.pikachu.position.x - 1;
            
            //Check if pikachu touches the wall
            if(self.pikachu.position.x <= 0){
                self.pikachuDirection  = "right"
            }
        }
        
//        //2. Automatic Movement  (Built-in SpriteKit Functions)
//
//        let upMoveAction = SKAction.moveBy(x:0, y:1, duration:2)
//        let leftMoveAction = SKAction.moveBy(x: -1, y: 0, duration: 2)
//        self.square.run(upMoveAction)
//        self.highScoreLabel.run(leftMoveAction)
        
      
        
    }
    
    
    //DETECTS USER INPUT
    //------------------------------------
    //touchesBEGAN == Event.ACTION_DOWN
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    //touchesENDED == Event.ACTION_UP  --> when finger touches the screen
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Person touches the screen")
        
        //GET THE (x,y) Position of the mouse and Sprite KIt will return the psotion as a UITouch Object
        let locationTouched = touches.first
        if(locationTouched == nil){
            //some errror detected while touching the location
            return
        }
        
        //Inside the UITouch Object, look at the location property
        //The location property
        let mousePosition = locationTouched!.location(in:self)
        print("x=\(mousePosition.x)")
        print("y=\(mousePosition.y)")
        print("--------")
    }

}

